const mongoose = require('mongoose');



async function connectDB(){

	return mongoose.connect(process.env.MONGODB_URI, {useCreateIndex: true, useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false });

}




function valida_null(field){

	if(field === undefined || field === "" || field == "null" || field == "NULL"){
		return null;
	}else{
		return field;
	}

}




function valida_field(field, label){

	if ( valida_null(field) === null ) {

        return {
			"status": {
				"code": 1,
				"message": "The field " + label + " is required"
			},
			"data": null
		}

    }else{

        return {
			"status": {
				"code": 0,
				"message": ""
			},
            "data": null
        };

    }

}




function response(code, message, response){

	return {
		"status": {
			"code": code,
			"message": message
		},
		"data": response
	};

}




module.exports = {
	connectDB,
	valida_null, 
	valida_field,
	response
};