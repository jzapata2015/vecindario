const mongoose = require("mongoose");

const Post = mongoose.model("Post", new mongoose.Schema({
    email:        { type: String, required: true  },
    description:  { type: String, required: true },
    color:        { type: String, required: true  },
    date:         { type: Date, 'default': Date.now },
    comments:     [
        {
            email:      { type: String, required: true },
            comment:    { type: String, required: true },
            date:       { type: Date, 'default': Date.now }
        }
    ],
    likes:        { type: Number, required: true, 'default': 0 },
    dislikes:     { type: Number, required: true, 'default': 0 }
}));

module.exports = Post;