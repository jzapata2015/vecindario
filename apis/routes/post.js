const express = require('express');

const Post = require('../models/Post');


//UTILS
const { connectDB, valida_field, valida_null, response } = require('../utils');


//RUTAS
const router = express.Router();





/************************************************************ POST *************************************************************/
router.get('/page/:page?', async (req, res) => {

    const { page } = req.params;

    const current_page = page || 0;

    const registers_page = 5;

    const skip = ( parseInt(current_page) ) * registers_page;



    connectDB();



    Post.find({})
    .limit( parseInt(registers_page) )
    .skip( parseInt(skip) )
    .sort([['date', -1]])
    .exec()
    .then(
        (result) => {
            return res.status(200).send( response(0, "List post successful", result ) );
        }
    )
    .catch( (error) => {
        return res.status(200).send( response(1, "Error", error) );
    });

});






router.get('/detail/:id', async (req, res) => {

    const { id } = req.params;



    connectDB();



    Post.findOne({ _id: id })
    .exec()
    .then(
        (result) => {
            return res.status(200).send( response(0, "Detail post", result ) );
        }
    )
    .catch( (error) => {
        return res.status(200).send( response(1, "Error get detail post", error) );
    });

});







router.post('/', async (req, res) => {

    /****************** CAPTURA DE PARAMETROS  ******************/
    var validate;
    var { email, description } = req.body;

    validate = valida_field( email, "email");
    if( validate.status.code === 1 ){ return res.status(200).send( validate ); }

    validate = valida_field( description, "description");
    if( validate.status.code === 1 ){ return res.status(200).send( validate ); }

    const colors = ["#90caf9", "#42a5f5", "#ce93d8", "#ab47bc", "#e57373", "#f44336", "#d32f2f", "#ffb74d", "#ffa726", "#f57c00", "#29b6f6", "#0288d1", "#81c784", "#66bb6a", "#388e3c"];
    const color_selected = Math.floor(Math.random() * (14 - 0)) + 0;
    const color = colors[ color_selected ];


    connectDB();



    Post.create({
        email: email,
        description: description,
        color: color
    })
    .then( (result) => {
        return res.status(200).send( response(0, "Created post successful", result ) );
    } )
    .catch( (error) => {
        return res.status(200).send( response(1, "Error creating post", error) );
    });


});






/************************************************************ COMMENT *************************************************************/
router.get('/detail/:id/comment', async (req, res) => {

    /****************** CAPTURA DE PARAMETROS  ******************/
    var validate;
    var { id } = req.params;

    validate = valida_field( id, "id");
    if( validate.status.code === 1 ){ return res.status(200).send( validate ); }


    connectDB();


    Post.findOne({ _id: id })
    .then( (result) => {
        return res.status(200).send( response(0, "List comments", result.comments ) );
    } )
    .catch( (error) => {
        return res.status(200).send( response(1, "Error listing comments", error) );
    });


});







router.post('/detail/:id/comment', async (req, res) => {

    /****************** CAPTURA DE PARAMETROS  ******************/
    var validate;
    var { id } = req.params;
    var { email, comment } = req.body;

    validate = valida_field( id, "id");
    if( validate.status.code === 1 ){ return res.status(200).send( validate ); }

    validate = valida_field( email, "email");
    if( validate.status.code === 1 ){ return res.status(200).send( validate ); }

    validate = valida_field( comment, "comment");
    if( validate.status.code === 1 ){ return res.status(200).send( validate ); }



    connectDB();


    const query = { _id: id };

    const update = {
        "$push": {
            "comments": {
                "email": email,
                "comment": comment
            }
        }
    };

    const options = { "upsert": false };



    Post.updateOne(query, update, options)
    .then( (result) => {
        return res.status(200).send( response(0, "Comment create succesfull", {
            "email": email,
            "comment": comment,
            "date": new Date()
        } ) );
    } )
    .catch( (error) => {
        return res.status(200).send( response(1, "Error creating comment", error) );
    });


});






/************************************************************ LIKE / DISLIKE *************************************************************/
router.put('/detail/:id/like', async (req, res) => {

    /****************** CAPTURA DE PARAMETROS  ******************/
    var validate;
    var { id } = req.params;
    var { type } = req.body;

    validate = valida_field( id, "id");
    if( validate.status.code === 1 ){ return res.status(200).send( validate ); }

    validate = valida_field( type, "type");
    if( validate.status.code === 1 ){ return res.status(200).send( validate ); }

    var query = {};
    if(type == "sum"){
        query = { $inc: { likes: +1 } };
    } else {
        query = { $inc: { likes: -1 } };
    }


    connectDB();


    Post.updateOne({"_id": id}, query)
    .then( (result) => {
        return res.status(200).send( response(0, "Like succesful", {} ) );
    } )
    .catch( (error) => {
        return res.status(200).send( response(1, "Error Liking", error) );
    });


});





router.put('/detail/:id/dislike', async (req, res) => {

    /****************** CAPTURA DE PARAMETROS  ******************/
    var validate;
    var { id } = req.params;
    var { type } = req.body;

    validate = valida_field( id, "id");
    if( validate.status.code === 1 ){ return res.status(200).send( validate ); }

    validate = valida_field( type, "type");
    if( validate.status.code === 1 ){ return res.status(200).send( validate ); }

    var query = {};
    if(type == "sum"){
        query = { $inc: { dislikes: +1 } };
    } else {
        query = { $inc: { dislikes: -1 } };
    }


    connectDB();


    Post.updateOne({"_id": id}, query)
    .then( (result) => {
        return res.status(200).send( response(0, "Dislikes succesful", {} ) );
    } )
    .catch( (error) => {
        return res.status(200).send( response(1, "Error disliking", error) );
    });

});


module.exports = router;