"use strict"

 var assert = require('assert');
 var request = require('supertest')
 var app = require('../index.js')

 var request = request("http://localhost:3001")


 describe('Posts', function() {

     describe('GET', function(){

        it('Should return array of posts', function(done){
             request.get('/post/page/1')
                 .expect('Content-Type', /json/)
                 .expect(200, done);
         });

     });


     describe('POST', function(){
         
        it('Should return 200 status code when posts is create', function(done){

            let body = {
                "email": "usuario_31@gmail.com",
                "description": "Post de prueba 31"
            };

            request.post('/post/')
                .send(body)
                .expect('Content-Type', /json/)
                .expect(200, done);

         });

     });


     describe('GET', function(){

        it('Should return object with info post', function(done){
             request.get('/post/detail/615e7691ab91f11f2b470ec9')
                 .expect('Content-Type', /json/)
                 .expect(200, done);
         });

     });


     describe('PUT', function(){
         
        it('Should return 200 status code when like is checked', function(done){

            let body = { "type": "sum" };

            request.put('/post/detail/615e7691ab91f11f2b470ec9/like')
                .send(body)
                .expect('Content-Type', /json/)
                .expect(200, done);

         });

     });


     describe('PUT', function(){
         
        it('Should return 200 status code when like is unchecked', function(done){

            let body = { "type": "min" };

            request.put('/post/detail/615e7691ab91f11f2b470ec9/like')
                .send(body)
                .expect('Content-Type', /json/)
                .expect(200, done);

         });

     });


     describe('PUT', function(){
         
        it('Should return 200 status code when dislike is checked', function(done){

            let body = { "type": "sum" };

            request.put('/post/detail/615e7691ab91f11f2b470ec9/dislike')
                .send(body)
                .expect('Content-Type', /json/)
                .expect(200, done);

         });

     });


     describe('PUT', function(){
         
        it('Should return 200 status code when dislike is unchecked', function(done){

            let body = { "type": "min" };

            request.put('/post/detail/615e7691ab91f11f2b470ec9/dislike')
                .send(body)
                .expect('Content-Type', /json/)
                .expect(200, done);

         });

     });

 });




 
describe('Comments', function() {

    describe('GET', function(){

       it('Should return array of comments of posts', function(done){
            request.get('/post/detail/615e7691ab91f11f2b470ec9/comment')
                .expect('Content-Type', /json/)
                .expect(200, done);
        });

    });


    describe('POST', function(){
        
       it('Should return 200 status code when posts is create', function(done){

           let body = {
                "email": "testunit@gmail.com",
                "comment": "Comment test unit"
            }

           request.post('post/detail/615e7691ab91f11f2b470ec9/comment')
               .send(body)
               .expect('Content-Type', /json/)
               .expect(200, done);

        });

    });


});
