//LIBRERIAS
const express       = require('express');
const cors          = require('cors');
const serverless    = require('serverless-http');


//CARGAR EL .ENV CUANDO SE TIENEN PROBLEMAS
const dotenv = require('dotenv');
dotenv.config();


//EXPRESS INICIALIZATION
const app = express();
app.use(cors());
app.use(express.urlencoded({ extended: true }));
app.use(express.json());


//RUTAS APP
app.use('/post', require('./routes/post'));


//PARA LOCALHOST
app.listen(process.env.PORT, ()=> { console.log(`---Server running on ${process.env.PORT}---`); });

//PARA PRODUCCION
//module.exports.handler = serverless(app);
