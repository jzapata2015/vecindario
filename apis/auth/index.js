const jwt = require('jsonwebtoken');
const { connectDB, response } = require('../utils');
const User = require('../models/User');



const isAuthenticated = ( req, res, next ) => {

    const token = req.headers.authorization;

    if (!token) {
        return res.status(403).send( { error: true, message: "Token is missing", data: null } );
    } else {

        jwt.verify(token, process.env.JWT_SECRET_KEY, async (error, decoded) => {

            if( error ) {

                if(error.name == "TokenExpiredError"){
                    return res.status(403).send( { error: true, message: "Token expired", data: null } );
                } else {
                    return res.status(403).send( { error: true, message: "Token Invalid", data: null } );
                }

            } else {

                const { userId } = decoded;
                connectDB();

                try{

                    const user = await User.find({_id: userId}).exec();

                    if( user.length > 0 ){
                        req.user = user[0];
                        next();
                    } else {
                        return res.status(403).send( { error: true, message: "Token Wrong", data: null } );
                    }

                } catch(error){
                    console.log(error);
                    return res.status(200).send( response(1, "Error consulting token", error) );
                }

            }

        });

    }

}


module.exports = {
    isAuthenticated
}