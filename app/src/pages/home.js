import React, { useState, useEffect } from 'react'
import { makeStyles } from '@material-ui/styles';
import '../App.css';
import CardViewPost from '../components/CardViewPost';


import { connect } from 'react-redux';
import { actions } from '../store/modules';


const useStyles = makeStyles((theme) => ({
  page: {
    paddingTop: 20,
    backgroundColor: "#282c34",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    minHeight: '80vh'
  }
}));



function Home( props ) {

  const classes = useStyles();
  
  const { actionClean, actionList, stateApp, statePostList } = props;
  const [ page, setPage ] = useState(0);


  useEffect(() => {

    actionClean();

    actionList(page).then( (response) => {
      if(response.error){
        alert(response.errorMessage);
      }
    });

    setTimeout( () => {
      window.addEventListener("scroll", handleScroll);
    }, 1000);

  }, []);


  useEffect( () => {
    handleGetData(page);
  }, [page]);






  const handleGetData = () => {

    actionList(page).then( (response) => {
      if(response.error){
        alert(response.errorMessage);
      }
    });

  }


  const handleScroll = () => {
    
    let userScrollHeight = window.innerHeight + window.scrollY;
    let windowBottomHeight = document.documentElement.offsetHeight;
    
    if (userScrollHeight >= windowBottomHeight) {
      setPage(prev => prev + 1);
    }

  }


  const handleCallback = (id, event, checked) => {

    if(event === "like"){

      statePostList.map( (post) => {
        if(post._id === id){
          post.likes = (checked) ? post.likes + 1 : post.likes - 1;
        }
        return post;
      });

    }

    if(event === "dislike"){
      statePostList.map( (post) => {
        if(post._id === id){
          post.dislikes = (checked) ? post.dislikes + 1 : post.dislikes - 1;
        }
        return post;
      });
    }

    if(event === "comments"){
      statePostList.map( (post) => {
        if(post._id === id){
          post.comments = post.comments + 1;
        }
        return post;
      });
    }

  }


  return (
    <div className={classes.page}>

      {
        statePostList.map((item)=>{

          return <CardViewPost
                    key={item._id}
                    id={item._id}
                    email={item.email}
                    description={item.description}
                    color={item.color}
                    date={item.date}
                    letter={(item.email).charAt(0)}
                    comments={item.comments}
                    likes={item.likes}
                    likeChecked={ stateApp.likes.includes(item._id) }
                    dislikes={item.dislikes}
                    dislikeChecked={ stateApp.dislikes.includes(item._id) }
                    callback={handleCallback}
                    clickeable={true}
                />

        })
      }

    </div>
  );
}


const mapStateToProps = state => ({
  stateApp:       state.app,
  statePostList:  state.post.data,
});


const mapDispatchToProps = dispatch => ({
  actionClean: () => dispatch( actions.post.clean()),
  actionList:  ( page ) => dispatch( actions.post.list( page ))
});


export default connect( mapStateToProps, mapDispatchToProps ) (Home);