import * as React from 'react';
import { makeStyles } from '@material-ui/styles';
import '../App.css';
import { useParams } from "react-router-dom";
import CardViewPost from '../components/CardViewPost';

import Fab from '@mui/material/Fab';
import KeyboardBackspaceIcon from '@mui/icons-material/KeyboardBackspace';

import { history }  from '../components/History';


import { connect } from 'react-redux';
import { actions } from '../store/modules';


const useStyles = makeStyles((theme) => ({
  page: {
    paddingTop: 20,
    backgroundColor: "#282c34",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    minHeight: '80vh'
  }
}));



function PostDetail(props) {
  
  let { id } = useParams();

  const classes = useStyles();

  const { stateApp, stateCommentList, actionDetail, actionComments } = props;
  const [ post, setPost ] = React.useState(null);

  const handleGetPost = () => {
    
    actionDetail(id).then( (response) => {

      if(response.error){
        alert(response.errorMessage);
      } else {
        setPost(response.data);
      }

    });

  };



  const handleGetComments = () => {
    
    actionComments(id).then( (response) => {

      if(response.error){
        alert(response.errorMessage);
      }

    });

  };


  React.useEffect( () => {

    handleGetPost();
    handleGetComments();

  }, []);







  const handleCallback = (id, event, checked) => {

    if(event === "like"){
      if(post._id === id){
        post.likes = (checked) ? post.likes + 1 : post.likes - 1;
      }
    }

    if(event === "dislike"){
      if(post._id === id){
        post.dislikes = (checked) ? post.dislikes + 1 : post.dislikes - 1;
      }
    }

    if(event === "comments"){
      if(post._id === id){
        post.comments = post.comments + 1;
      }
    }

    setPost(post);

  }


  return (
    <div className={classes.page}>

      {
        (post != null) ?

        <CardViewPost
            key={post._id}
            id={post._id}
            email={post.email}
            description={post.description}
            color={post.color}
            date={post.date}
            letter={(post.email).charAt(0)}
            comments={post.comments}
            likes={post.likes}
            likeChecked={ stateApp.likes.includes(post._id) }
            dislikes={post.dislikes}
            dislikeChecked={ stateApp.dislikes.includes(post._id) }
            callback={handleCallback}
            clickeable={false}
            commentsList={stateCommentList}
          />

        : null
      }

        <Fab aria-label="add" style={ { backgroundColor: "#FDDC05", position: "fixed", bottom: "10%", left: 10} } onClick={ () => { history.goBack(); } }>
          <KeyboardBackspaceIcon />
        </Fab>

    </div>
  );
}



const mapStateToProps = state => ({
  stateApp:           state.app,
  stateCommentList:   state.comment.data
});


const mapDispatchToProps = dispatch => ({
  actionDetail:     ( postId ) => dispatch( actions.post.detail( postId )),
  actionComments:   ( postId ) => dispatch( actions.comment.list( postId ))
});


export default connect( mapStateToProps, mapDispatchToProps ) (PostDetail);