import * as React from 'react';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import { Formik, Field } from 'formik';
import * as Yup from 'yup';


import { connect } from 'react-redux';
import { actions } from '../store/modules';


const theme = createTheme({
  palette: {
    mode: 'dark',
    primary: {
      main: "#ffcc00",
    },
    secondary: {
      main: '#FFFFFF'
    },
  },
});


function PostNew(props) {

  const { actionCreate, dialogPosthandleClose } = props;


  const DisplayingErrorMessagesSchema = Yup.object().shape({
    email: Yup.string().email('Correo invalido').required('Requerido'),
    description: Yup.string().required('Requerido')
  });


  return (
    <ThemeProvider theme={theme}>

      <Formik
        initialValues={{
          email: "",
          description: ""
        }}
        validationSchema={ DisplayingErrorMessagesSchema }
        validateOnChange={false}
        validateOnBlur={false}
        onSubmit={(form, { resetForm }) => {

          actionCreate( { email: form.email, description: form.description } ).then( (response) => {

            if(response.error){
              alert(response.errorMessage);
            } else {
              resetForm();
              dialogPosthandleClose();
            }

          });

        }}
      >
  
        {({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit
        }) => (

          <Dialog open={props.dialogPostOpen} onClose={props.dialogPosthandleClickOpen}>

            <DialogTitle style={ { fontWeight: "700" } }>Publicar Post</DialogTitle>
            
            <DialogContent>

              <Field
                component={TextField}
                margin="dense"
                id="email"
                name="email"
                label="Email"
                type="email"
                fullWidth={true}
                variant="standard"
                helperText={ errors.email }
                error={errors.email}
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.email}
              />

              <Field
                component={TextField}
                margin="dense"
                id="description"
                name="description"
                label="Descripcion"
                type="text"
                fullWidth={true}
                variant="standard"
                multiline
                minRows={3}
                helperText={errors.description}
                error={errors.description}
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.description}
              />

            </DialogContent>

            <DialogActions>
              <Button onClick={dialogPosthandleClose} color="error">Cancelar</Button>
              <Button color="success" onClick={handleSubmit}>Publicar</Button>
            </DialogActions>

          </Dialog>

        )}

      </Formik>

    </ThemeProvider>
  );
}


const mapStateToProps = state => ({
  stateLoading: state.app.loading
});


const mapDispatchToProps = dispatch => ({
  actionCreate:  ( data ) => dispatch( actions.post.create( data ))
});


export default connect( mapStateToProps, mapDispatchToProps ) (PostNew);