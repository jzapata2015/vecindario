
import { createTheme, ThemeProvider } from '@mui/material/styles';
import GroupIcon from '@mui/icons-material/Group';


const theme = createTheme({
  palette: {
    primary: {
      main: "#FFFFFF",
    },
    secondary: {
      main: "#FDDC05",
    },
  },
});


function Group() {
  return (
    <div className="App">

      <ThemeProvider theme={theme}>

        <header className="App-header">
          <GroupIcon color="secondary" sx={ { fontSize: { lg: 200, md: 150, sm: 150, xs: 100 } } }/>
          <p style={ { fontSize: 30, fontWeight: "700", color: "#FDDC05"} }>MENSAJES PROXIMAMENTE ...</p>
        </header>

      </ThemeProvider>

    </div>
  );
}

export default Group;