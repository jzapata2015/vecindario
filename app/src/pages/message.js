
import { createTheme, ThemeProvider } from '@mui/material/styles';
import Sms from '@mui/icons-material/Sms';


const theme = createTheme({
  palette: {
    primary: {
      main: "#FFFFFF",
    },
    secondary: {
      main: "#FDDC05",
    },
  },
});


function Message() {
  return (
    <div className="App">

      <ThemeProvider theme={theme}>

        <header className="App-header">
          <Sms color="secondary" sx={ { fontSize: { lg: 200, md: 150, sm: 150, xs: 100 } } }/>
          <p style={ { fontSize: 30, fontWeight: "700", color: "#FDDC05"} }>MENSAJES PROXIMAMENTE ...</p>
        </header>

      </ThemeProvider>

    </div>
  );
}

export default Message;