
import { createTheme, ThemeProvider } from '@mui/material/styles';
import AccountBox from '@mui/icons-material/AccountBox';


const theme = createTheme({
  palette: {
    primary: {
      main: "#FFFFFF",
    },
    secondary: {
      main: "#FDDC05",
    },
  },
});


function Profile() {
  return (
    <div className="App">

      <ThemeProvider theme={theme}>

        <header className="App-header">
          <AccountBox color="secondary" sx={ { fontSize: { lg: 200, md: 150, sm: 150, xs: 100 } } }/>
          <p style={ { fontSize: 30, fontWeight: "700", color: "#FDDC05"} }>PERFIL PROXIMAMENTE ...</p>
        </header>

      </ThemeProvider>

    </div>
  );
}

export default Profile;