import * as React from 'react';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import { Formik, Field } from 'formik';
import * as Yup from 'yup';


import { connect } from 'react-redux';
import { actions } from '../store/modules';


const theme = createTheme({
  palette: {
    mode: 'dark',
    primary: {
      main: "#ffcc00",
    },
    secondary: {
      main: '#FFFFFF'
    },
  },
});


function CommentNew(props) {

  const { actionCreate, dialogCommentHandleClose, postId, callback } = props;

  const DisplayingErrorMessagesSchema = Yup.object().shape({
    email: Yup.string().email('Correo invalido').required('Requerido'),
    comment: Yup.string().required('Requerido')
  });


  return (
    <ThemeProvider theme={theme}>

      <Formik
        initialValues={{
          email: "",
          comment: ""
        }}
        validationSchema={ DisplayingErrorMessagesSchema }
        validateOnChange={false}
        validateOnBlur={false}
        onSubmit={(form, { resetForm }) => {

          actionCreate( postId, { email: form.email, comment: form.comment } ).then( (response) => {

            if(response.error){
              alert(response.errorMessage);
            } else {
              resetForm();
              dialogCommentHandleClose();
              callback(postId, "comments", true);
            }

          });

        }}
      >
  
        {({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit
        }) => (

          <Dialog open={props.dialogCommentOpen} onClose={props.dialogCommentHandleClickOpen}>
            
            <DialogTitle style={ { fontWeight: "700" } }>Comentar</DialogTitle>
            
            <DialogContent>

              <Field
                component={TextField}
                margin="dense"
                id="email"
                name="email"
                label="Email"
                type="email"
                fullWidth={true}
                variant="standard"
                helperText={ errors.email }
                error={errors.email}
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.email}
              />

              <Field
                component={TextField}
                margin="dense"
                id="comment"
                name="comment"
                label="Comentario"
                type="text"
                fullWidth={true}
                variant="standard"
                multiline
                minRows={3}
                helperText={ errors.comment }
                error={errors.comment}
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.comment}
              />

            </DialogContent>

            <DialogActions>
              <Button onClick={dialogCommentHandleClose} color="error">Cancelar</Button>
              <Button onClick={handleSubmit} color="success">Comentar</Button>
            </DialogActions>

          </Dialog>

        )}

      </Formik>

    </ThemeProvider>
  );
}


const mapStateToProps = state => ({
});


const mapDispatchToProps = dispatch => ({
  actionCreate:  ( postId, data ) => dispatch( actions.comment.create( postId, data ))
});


export default connect( mapStateToProps, mapDispatchToProps ) (CommentNew);