import React from 'react';
import PropTypes from 'prop-types';
import { CssBaseline } from '@mui/material';
import { makeStyles } from '@material-ui/styles';

import Navbar from './layouts//Navbar';
import Content from './layouts/Content';
import Footer from './layouts/Footer';

import { connect } from 'react-redux';
import Loader from './components/Loader';


const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: "column"
  }
}));


function App(props) {

  const { stateLoading } = props;

  const classes = useStyles();

  return (
    <main className={classes.root}>
  
      { stateLoading ? <Loader /> : <></> }

      <CssBaseline />

      <Navbar/>

      <Content />

      <Footer />
    </main>
  );
}


App.propTypes = {
  window: PropTypes.func,
};




const mapStateToProps = state => ({
    stateLoading: state.app.loading
});


const mapDispatchToProps = dispatch => ({
});


export default connect( mapStateToProps, mapDispatchToProps ) (App);