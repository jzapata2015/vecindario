import * as React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';


export default function CardViewComment(props) {

  return (
    <Card
      sx={{
        width: {
          xs: 350,
          sm: 500,
          md: 600,
          lg: 700,
          xl: 800,
        }
      }}
      style={ { marginBottom: 10, opacity: 0.7 } }
    >

      <CardContent>
        <Typography gutterBottom variant="h8" component="div">
          {props.mail}
        </Typography>
        <Typography gutterBottom variant="h8" component="div" style={ { opacity: 0.5 } }>
          {props.date}
        </Typography>
        <Typography variant="body2" color="text.secondary">
          {props.comment}
        </Typography>
      </CardContent>
  
    </Card>
  );
}