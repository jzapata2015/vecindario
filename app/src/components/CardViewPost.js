import * as React from 'react';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import MessageIcon from '@mui/icons-material/Message';

import Avatar from '@mui/material/Avatar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';

import LikeIcon from '@mui/icons-material/ThumbUp';
import DisLikeIcon from '@mui/icons-material/ThumbDownAlt';

import { history } from './History';
import CommentNew from '../pages/commentNew';
import CardViewComment from '../components/CardViewComment';

import { connect } from 'react-redux';
import { actions } from '../store/modules';



function CardViewPost(props) {

  const {
    id,
    email,
    description,
    date,
    color,
    letter,
    likes,
    dislikes,
    comments,
    likeChecked,
    dislikeChecked,
    clickeable,
    actionLike,
    actionDislike,
    callback,
    commentsList
  } = props;

  const [dialogCommentOpen, setDialogCommentOpen] = React.useState(false);


  const dialogCommentHandleClickOpen = () => {
    setDialogCommentOpen(true);
  };


  const dialogCommentHandleClose = () => {
    setDialogCommentOpen(false);
  };


  const handleLike = (id) => {

    callback(id, "like", !likeChecked);

    actionLike(id, ((likeChecked)?'min':'sum') ).then( (response) => {

    });

  };


  const handleDislike = (id) => {
    
    callback(id, "dislike", !dislikeChecked);

    actionDislike(id, ((dislikeChecked)?'min':'sum')).then( (response) => {

    });

  };


  return (
    <div>

      <Card
        sx={{
          width: {
            xs: 350,
            sm: 500,
            md: 600,
            lg: 700,
            xl: 800,
          }
        }}
        style={ { marginBottom: 10 } }
      >

        <CardHeader
          avatar={
            <Avatar sx={{ bgcolor: color }} aria-label="recipe">
              { (letter).toUpperCase() }
            </Avatar>
          }
          title={email}
          subheader={date}
        />

        <CardContent style={
            {
              display: "flex",
              height: 200,
              backgroundColor: color,
              justifyContent: "center",
              alignItems: "center",
              overflow: "hidden",
              cursor: (clickeable) ? "pointer" : "auto"
            }
          }
          onClick={ () => { if ( clickeable ) { history.push("/post/" + id ); } } }
        >
            <div style={
                {
                  fontSize: 30,
                  fontWeight: "700",
                  textAlign: "center",
                  fontStyle: "italic"
                }
              }
            >
              "{description}"
            </div>
        </CardContent>

        <CardActions disableSpacing style={ { flexDirection: "row", justifyContent: "space-evenly" }}>

          <IconButton aria-label="add to favorites" onClick={() => { handleLike(id) } } >
            <Typography style={ { marginRight: 5 } }>{likes}</Typography>
            <LikeIcon  sx={{ color: (likeChecked) ? "#2E86C1" : "#566573" }} />
          </IconButton>

          <IconButton aria-label="share" onClick={() => { handleDislike(id) } }>
            <Typography style={ { marginRight: 5 } }>{dislikes}</Typography>
            <DisLikeIcon sx={{ color: (dislikeChecked) ? "#C70039" : "#566573" }} />
          </IconButton>

          <IconButton aria-label="comments" onClick={dialogCommentHandleClickOpen}>
            <Typography style={ { marginRight: 5 } }>{comments}</Typography>
            <MessageIcon sx={{ color: "#566573" }} />
          </IconButton>

        </CardActions>

        <CommentNew
          dialogCommentOpen={dialogCommentOpen}
          dialogCommentHandleClickOpen={dialogCommentHandleClickOpen}
          dialogCommentHandleClose={dialogCommentHandleClose}
          postId={id}
          callback={callback}
        />

      </Card>


  
      {
        (commentsList) ?
          
          commentsList.map((comment)=>{
            return  <CardViewComment
                      id={comment._id}
                      mail={comment.email}
                      date={comment.date}
                      comment={comment.comment}
                    />
          })

        : null
      }

    </div>

  );
}


const mapStateToProps = state => ({
  stateLoading: state.app.loading
});


const mapDispatchToProps = dispatch => ({
  actionLike:           (id, type) => dispatch( actions.post.like(id, type)),
  actionDislike:        (id, type) => dispatch( actions.post.dislike(id, type))
});


export default connect( mapStateToProps, mapDispatchToProps ) (CardViewPost);