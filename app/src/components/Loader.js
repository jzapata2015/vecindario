import React from 'react';
import { Backdrop, CircularProgress } from '@mui/material';
import { makeStyles } from '@material-ui/styles';

const useStyles = makeStyles((theme) => ({
  backdrop: {
    zIndex: 10,
    color: '#fff'
  }
}));


export default function SimpleBackdrop() {

  const classes = useStyles();
  const [open, setOpen] = React.useState(true);

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
        <Backdrop className={classes.backdrop} open={open} onClick={handleClose}>
          <CircularProgress color="inherit" />
        </Backdrop>
    </div>
  );

}