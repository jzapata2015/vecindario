import React from 'react';
import { makeStyles } from '@material-ui/styles';
import { createTheme, ThemeProvider } from '@mui/material/styles';

import { Router, Route, Switch } from "react-router-dom";
import { history }  from '../components/History';


import HomePage from "../pages/home";
import PostPage from "../pages/postNew";
import PostDetailPage from "../pages/postDetail";
import LoginPage from "../pages/login";


import ProfilePage from "../pages/profile";
import MessagePage from "../pages/message";
import GroupPage from "../pages/group";

const theme = createTheme({
  palette: {
    primary: {
      main: "#FDDC05",
    },
    secondary: {
      main: "#FFFFFF",
    },
  },
});


const useStyles = makeStyles({
  content: {
    flexGrow: 1,
    marginTop: 50
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end'
  }
});


export default function Content(props) {

    const classes = useStyles();

    return (
        <main className={classes.content}>

          <ThemeProvider theme={theme}>

            <Router history={history}>
              
                <Switch>

                  <Route path="/login">
                    <LoginPage />
                  </Route>

                  <Route exact path="/post">
                    <PostPage />
                  </Route>

                  <Route exact path="/post/:id">
                    <PostDetailPage />
                  </Route>

                  <Route path="/profile">
                    <ProfilePage />
                  </Route>

                  <Route path="/message">
                    <MessagePage />
                  </Route>

                  <Route path="/group">
                    <GroupPage />
                  </Route>

                  <Route path="/">
                    <HomePage />
                  </Route>

                </Switch>

            </Router>

          </ThemeProvider>
  
        </main>
    );

}