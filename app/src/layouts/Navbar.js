import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import MenuItem from '@mui/material/MenuItem';
import Menu from '@mui/material/Menu';
import MenuIcon from '@mui/icons-material/Menu';
import AccountCircle from '@mui/icons-material/AccountCircle';

import ArrowLeftIcon from '@mui/icons-material/ArrowLeft';
import MailIcon from '@mui/icons-material/Mail';

import { history } from '../components/History';
import PostNew from '../pages/postNew';


import { connect } from 'react-redux';


const theme = createTheme({
  palette: {
    primary: {
      main: "#FFFFFF",
    },
    secondary: {
      main: "#FDDC05",
    },
  },
});



function NavBar (props) {

  const [anchorEl, setAnchorEl] = React.useState(null);
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = React.useState(null);

  const [dialogPostOpen, setDialogPostOpen] = React.useState(false);

  const dialogPosthandleClickOpen = () => {
    setDialogPostOpen(true);
  };

  const dialogPosthandleClose = () => {
    setDialogPostOpen(false);
  };


  const isMenuOpen = Boolean(anchorEl);
  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

  const handleProfileMenuOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMobileMenuClose = () => {
    setMobileMoreAnchorEl(null);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
    handleMobileMenuClose();
  };

  const handleMobileMenuOpen = (event) => {
    setMobileMoreAnchorEl(event.currentTarget);
  };

  const menuId = 'primary-search-account-menu';

  const renderMenu = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{
        vertical: 'top',
        horizontal: 'right',
      }}
      id={menuId}
      keepMounted
      transformOrigin={{
        vertical: 'top',
        horizontal: 'right',
      }}
      open={isMenuOpen}
      onClose={handleMenuClose}
    >
      <MenuItem onClick={ () => { handleMenuClose(); history.push("/profile"); }}> <ArrowLeftIcon /> Mi Perfil</MenuItem>
      <MenuItem onClick={ () => { handleMenuClose(); history.push("/message"); }}> <ArrowLeftIcon /> Mensajes</MenuItem>
      <MenuItem onClick={ () => { handleMenuClose(); history.push("/group"); }}> <ArrowLeftIcon /> Grupos</MenuItem>
    </Menu>
  );



  const mobileMenuId = 'primary-search-account-menu-mobile';

  const renderMobileMenu = (
    <Menu
      anchorEl={mobileMoreAnchorEl}
      anchorOrigin={{
        vertical: 'top',
        horizontal: 'right',
      }}
      id={mobileMenuId}
      keepMounted
      transformOrigin={{
        vertical: 'top',
        horizontal: 'right',
      }}
      open={isMobileMenuOpen}
      onClose={handleMobileMenuClose}
    >

      <MenuItem onClick={dialogPosthandleClickOpen} >
        <IconButton size="large" aria-label="show 4 new mails" color="inherit">
          <MailIcon />
        </IconButton>
        <p>Post</p>
      </MenuItem>

      <MenuItem onClick={handleProfileMenuOpen}>
        <IconButton
          size="large"
          aria-label="account of current user"
          aria-controls="primary-search-account-menu"
          aria-haspopup="true"
          color="inherit"
        >
          <AccountCircle />
        </IconButton>
        <p>Javier</p>
      </MenuItem>

    </Menu>
  );


  return (
    <Box sx={{ flexGrow: 1, position: "fixed", top: 0, left: 0, width: "100%", zIndex: 2 }}>
      
      <ThemeProvider theme={theme}>
        
        <AppBar position="static" >

          <Toolbar>

            <img
              src="https://viewinmobiliario2.s3-sa-east-1.amazonaws.com/static_assets/vecindario-logo.svg"
              alt="vecindario-logo"
              style={ { width: 200, cursor: "pointer" } }
              onClick={ () => { history.push("/"); } }
            />
            
            <Box sx={{ flexGrow: 1 }} />

            <Box sx={{ display: { xs: 'none', md: 'flex' } }}>

            <MenuItem onClick={dialogPosthandleClickOpen} >
              <IconButton size="large" aria-label="show 4 new mails" color="inherit">
                <MailIcon />
              </IconButton>
              <p>Post</p>
            </MenuItem>

            <MenuItem onClick={handleProfileMenuOpen}>
              <IconButton
                size="large"
                edge="end"
                aria-label="account of current user"
                aria-controls={menuId}
                aria-haspopup="true"
                color="inherit"
              >
                <AccountCircle />
              </IconButton>
              <p>Javier</p>
            </MenuItem>

            </Box>
            <Box sx={{ display: { xs: 'flex', md: 'none' } }}>
              <IconButton
                size="large"
                aria-label="show more"
                aria-controls={mobileMenuId}
                aria-haspopup="true"
                onClick={handleMobileMenuOpen}
                color="inherit"
              >
                <MenuIcon />
              </IconButton>
            </Box>
          </Toolbar>
        </AppBar>
        
        {renderMobileMenu}

        {renderMenu}

      </ThemeProvider>



      <PostNew
        dialogPostOpen={dialogPostOpen}
        dialogPosthandleClickOpen={dialogPosthandleClickOpen}
        dialogPosthandleClose={dialogPosthandleClose}
      />

    </Box>
  );
};



const mapStateToProps = state => ({
    stateLoading: state.app.loading
});


const mapDispatchToProps = dispatch => ({
});


export default connect( mapStateToProps, mapDispatchToProps ) (NavBar);