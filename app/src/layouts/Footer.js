import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import Toolbar from '@mui/material/Toolbar';
import Container from '@mui/material/Container';


import { connect } from 'react-redux';


const theme = createTheme({
  palette: {
    primary: {
      main: "#FFFFFF",
    },
    secondary: {
      main: "#FDDC05",
    },
  },
});


function Footer (props) {

  return (
    <ThemeProvider theme={theme}>
      
      <AppBar position="static" color="primary">

        <Container maxWidth="lg">

          <Toolbar>

          <div style={ { width: "100%", textAlign: "center" } }>
              <span>Power by © 2021 Javier Zapata</span>
          </div>

          </Toolbar>

        </Container>

      </AppBar>

    </ThemeProvider>
  );
};



const mapStateToProps = state => ({
    stateLoading: state.app.loading
});


const mapDispatchToProps = dispatch => ({
});


export default connect( mapStateToProps, mapDispatchToProps ) (Footer);