import { app } from './app';
import { post } from './post';
import { comment } from './comment';


export const reducers = {
    app:        app.reducer,
    post:       post.reducer,
    comment:    comment.reducer
}


export const actions = {
    app:        app.actions,
    post:       post.actions,
    comment:    comment.actions
}


export const reducerPersistWhiteList = [
    'app'
];


export const reducerPersistBlackList = [
    'post', 'comment'
];


export { app, post, comment }