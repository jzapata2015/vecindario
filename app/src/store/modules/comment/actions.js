import  * as constants from './constants';
import { loading, sesionComment } from '../app/actions';
import { ApiBase } from '../../../constants';



export const list = ( postId ) => {

    return async (dispatch) => {

        var responseAction = { error: false, errorMessage: "", data: null };
        dispatch(loading(true));

        try {

            const fetch_response = await fetch( ApiBase + "/post/detail/" + postId + "/comment" , {
                method: 'GET',
                mode: 'cors',
                headers: {
                    'Access-Control-Allow-Origin': '*',
                    'Content-Type': 'application/json'
                }
            });

            if ( fetch_response.status === 200 ) {

                const response = await fetch_response.json();

                if( response.status.code === 0 ){
                    
                    responseAction.data = response.data;

                    dispatch({
                        type: constants.DATA,
                        payload: response.data
                    });
  
                } else {
                    responseAction.error = true;
                    responseAction.errorMessage = response.status.message;
                }

            } else {
                responseAction.error = true;
                responseAction.errorMessage = await fetch_response.text();
            }

        } catch (error) {
            responseAction.error = true;
            responseAction.errorMessage = error;
        }

        dispatch( loading(false) );
        return responseAction;

    }

}





export const create = (postId, data) => {

    return async (dispatch) => {

        var responseAction = { error: false, errorMessage: "", data: null };
        dispatch(loading(true));

        try {

            const fetch_response = await fetch( ApiBase + "/post/detail/" + postId + "/comment", {
                method: 'POST',
                mode: 'cors',
                headers: {
                    'Access-Control-Allow-Origin': '*',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify( data )
            });

            if ( fetch_response.status === 200 ) {

                const response = await fetch_response.json();

                if( response.status.code === 0 ){

                    responseAction.data = response.data;

                    dispatch({
                        type: constants.NEW_DATA,
                        payload: response.data
                    });

                    setTimeout( () => {
                        dispatch(sesionComment());
                    }, 500);

                } else {
                    responseAction.error = true;
                    responseAction.errorMessage = response.status.message;
                }

            } else {
                responseAction.error = true;
                responseAction.errorMessage = await fetch_response.text();
            }

        } catch (error) {
            responseAction.error = true;
            responseAction.errorMessage = error;
        }

        dispatch( loading(false) );
        return responseAction;

    }

}