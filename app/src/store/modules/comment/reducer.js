import  * as constants from './constants';

const initialState = {
  data: []
}


const reducer = ( state = initialState, action ) => {

  var new_data;

  switch ( action.type ) {

    case constants.DATA:

      new_data = action.payload;
      new_data.sort( (a,b) => new Date(b.date) - new Date(a.date) );

      return {
        ...state,
        data: new_data
      };

    case constants.NEW_DATA:

      new_data = [action.payload].concat( state.data );

      return {
        ...state,
        data: new_data
      };

    default: 
      return state;

  }

}


export default reducer;