import * as constants from './constants';
import { loading, sesionLike, sesionDislike } from '../app/actions';
import { ApiBase } from '../../../constants';



export const clean = () => {
    return async (dispatch) => {
        dispatch({ type: constants.CLEAN });
    }
}


export const list = (page) => {

    return async (dispatch) => {

        var responseAction = { error: false, errorMessage: "", data: null };
        dispatch(loading(true));

        try {

            const fetch_response = await fetch( ApiBase + "/post/page/" + page , {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json'
                }
            });

            if ( fetch_response.status === 200 ) {

                const response = await fetch_response.json();

                if( response.status.code === 0 ){

                    var data = response.data;
                    data.map( (obj) => { obj.comments = obj.comments.length; return true; } );

                    responseAction.data = data;
                    
                    dispatch({
                        type: constants.DATA,
                        payload: data
                    });
  
                } else {
                    responseAction.error = true;
                    responseAction.errorMessage = response.status.message;
                }

            } else {
                responseAction.error = true;
                responseAction.errorMessage = await fetch_response.text();
            }

        } catch (error) {
            responseAction.error = true;
            responseAction.errorMessage = error;
        }

        dispatch( loading(false) );
        return responseAction;

    }

}





export const create = (data) => {

    return async (dispatch) => {

        var responseAction = { error: false, errorMessage: "", data: null };
        dispatch(loading(true));

        try {

            const fetch_response = await fetch( ApiBase + "/post", {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify( data )
            });

            if ( fetch_response.status === 200 ) {

                const response = await fetch_response.json();

                if( response.status.code === 0 ){
                    responseAction.data = response.data;
                    
                    dispatch({
                        type: constants.NEW_DATA,
                        payload: response.data
                    });

                } else {
                    responseAction.error = true;
                    responseAction.errorMessage = response.status.message;
                }

            } else {
                responseAction.error = true;
                responseAction.errorMessage = await fetch_response.text();
            }

        } catch (error) {
            responseAction.error = true;
            responseAction.errorMessage = error;
        }

        dispatch( loading(false) );
        return responseAction;
    
    }

}





export const detail = (postId) => {

    return async (dispatch) => {

        var responseAction = { error: false, errorMessage: "", data: null };
        dispatch(loading(true));

        try {

            const fetch_response = await fetch( ApiBase + "/post/detail/" + postId, {
                method: 'GET',
                redirect: 'follow',
                mode: 'cors',
                headers: {
                    'Access-Control-Allow-Origin': '*',
                    'Content-Type': 'application/json'
                }
            });

            if ( fetch_response.status === 200 ) {

                const response = await fetch_response.json();

                if( response.status.code === 0 ){

                    var data = response.data;
                    data.comments = data.comments.length;
                    responseAction.data = data;

                } else {
                    responseAction.error = true;
                    responseAction.errorMessage = response.status.message;
                }

            } else {
                responseAction.error = true;
                responseAction.errorMessage = await fetch_response.text();
            }

        } catch (error) {
            responseAction.error = true;
            responseAction.errorMessage = error;
        }

        dispatch( loading(false) );
        return responseAction;

    }

}





export const like = (postId, type) => {

    return async (dispatch) => {

        var responseAction = { error: false, errorMessage: "", data: null };

        try {

            const fetch_response = await fetch( ApiBase + "/post/detail/" + postId + "/like", {
                method: 'PUT',
                headers: {
                    'Access-Control-Allow-Origin': '*',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify( { "type": type } )
            });

            if ( fetch_response.status === 200 ) {

                const response = await fetch_response.json();

                if( response.status.code === 0 ){
                    dispatch(sesionLike(postId));
                    responseAction.data = response.data;
                } else {
                    responseAction.error = true;
                    responseAction.errorMessage = response.status.message;
                }

            } else {
                responseAction.error = true;
                responseAction.errorMessage = await fetch_response.text();
            }

        } catch (error) {
            responseAction.error = true;
            responseAction.errorMessage = error;
        }

        return responseAction;

    }

}





export const dislike = (postId, type) => {

    return async (dispatch) => {

        var responseAction = { error: false, errorMessage: "", data: null };

        try {

            const fetch_response = await fetch( ApiBase + "/post/detail/" + postId + "/dislike", {
                method: 'PUT',
                headers: {
                    'Access-Control-Allow-Origin': '*',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify( { "type": type } )
            });

            if ( fetch_response.status === 200 ) {

                const response = await fetch_response.json();

                if( response.status.code === 0 ){
                    dispatch(sesionDislike(postId));
                    responseAction.data = response.data;
                } else {
                    responseAction.error = true;
                    responseAction.errorMessage = response.status.message;
                }

            } else {
                responseAction.error = true;
                responseAction.errorMessage = await fetch_response.text();
            }

        } catch (error) {
            responseAction.error = true;
            responseAction.errorMessage = error;
        }

        return responseAction;

    }

}