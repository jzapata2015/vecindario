import  * as constants from './constants';

const initialState = {
  data: []
}


const reducer = ( state = initialState, action ) => {
  
  var new_data;

  switch ( action.type ) {

    case constants.CLEAN:

      return {
        ...state,
        data: []
      };


    case constants.DATA:

      new_data = state.data.concat( action.payload );
      new_data = Object.values(new_data.reduce((acc,cur)=>Object.assign(acc,{[cur._id]:cur}),{}));

      return {
        ...state,
        data: new_data
      };


    case constants.NEW_DATA:

      new_data = [action.payload].concat( state.data );

      return {
        ...state,
        data: new_data
      };


    default: 
      return state;

  }

}


export default reducer;