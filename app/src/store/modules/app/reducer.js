import  * as constants from './constants';

const initialState = {
  loading: false,
  likes: [],
  dislikes: [],
  comment: false
}

const reducer = ( state = initialState, action ) => {

  switch ( action.type ) {

      case constants.LIKE:

        var new_likes = state.likes;

        if( new_likes.includes(action.payload) ) {
          new_likes = new_likes.filter(element => element !== action.payload);
        } else {
          new_likes.push(action.payload);
        }

        return {
          ...state,
          likes: new_likes
        };

      case constants.DISLIKE:

        var new_dislikes = state.dislikes;

        if( new_dislikes.includes(action.payload) ) {
          new_dislikes = new_dislikes.filter(element => element !== action.payload);
        } else {
          new_dislikes.push(action.payload);
        }

        return {
          ...state,
          dislikes: new_dislikes
        };

      case constants.LOADING:
          return {
            ...state,
            loading: action.payload
          };

      case constants.COMMENT:
          return {
            ...state,
            comment: !state.comment
          };

      default: 
          return state;

  }

}

export default reducer;