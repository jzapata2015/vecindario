import * as constants from './constants';



export const loading = payload => {

  return dispatch => {

    dispatch({
      type: constants.LOADING,
      payload
    });

  }

}



export const sesionLike = (payload) => {

  return dispatch => {

    dispatch({
      type: constants.LIKE,
      payload
    });

  }

}



export const sesionDislike = (payload) => {

  return dispatch => {

    dispatch({
      type: constants.DISLIKE,
      payload
    });

  }

}



export const sesionComment = (payload) => {

  return dispatch => {

    dispatch({
      type: constants.COMMENT,
      payload
    });

  }

}